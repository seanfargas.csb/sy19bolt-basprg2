#pragma once
#include "Buff.h"
class Concentration :
	public Buff
{
public:
	Concentration(int concentration);
	~Concentration();

private:
	int mConcentration;
};

