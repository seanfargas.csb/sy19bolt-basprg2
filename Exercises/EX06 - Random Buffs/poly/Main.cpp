#include <iostream>
#include <string>
#include <vector>
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"

using namespace std;

int main()
{
	Buff* buffs[5];
	buffs[0] = new Heal(100);
	buffs[1] = new Might(7);
	buffs[2] = new IronSkin(15);
	buffs[3] = new Concentration(8);
	buffs[4] = new Haste(5);

	while (true)
	{
		for (int i = 0; i < 5; i++)
		{
			Buff* buff =buffs[i];
			cout << buff->type() << " Skill" << buff->skill() << endl;
		}
	}
	system("pause");
	return 0;
}
