#pragma once
#include <string>
#include <iostream>
#include "Spell.h"
using namespace std;

class Wizard
{
public:

	Wizard();
	Wizard(string name, int hp, int mp, Spell* spell);

	//content

	string name;
	int hp;
	int mp;
	Spell*spell;

	void attack(Wizard*player, Wizard* enemy);
	void displayCurrent();
};