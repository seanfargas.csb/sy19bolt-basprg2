#include <iostream>
#include <conio.h>
#include <time.h>
#include <string>

using namespace std;

void integerArray(int *randPtr)
{
	for (int i = 0; i < 10; i++) 
	{
		*randPtr = rand() % 100 + 1;

		cout << *randPtr << " ";
	}
}
int main()
{
	srand(time(0));

	int randomInteger[10];
	int *randPtr = new int;

	integerArray(randPtr);

	delete[] randPtr;

	_getch();
}