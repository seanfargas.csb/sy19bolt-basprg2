#include <iostream>
#include <conio.h>
#include <time.h>
#include <string>

using namespace std;

void finalNum(int array[10])
{
	for (int i = 0; i < 10; i++) 
	{
		cout << array[i] << " ";
	}
}

int main()
{
	srand(time(0));

	int randNum[10];

	for (int x = 0; x < 10; x++) 
	{
		randNum[x] = rand() % 100 + 1;
	}
	finalNum(randNum);

	_getch();
}