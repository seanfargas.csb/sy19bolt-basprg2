#include <iostream>
#include <conio.h>

using namespace std;

/*
- The player starts with 1000 gold.
- The player bets gold that is not zero or greater than current gold.
- AI rolls the 2 dice. The sum of the 2 dice will be the value of his roll.
- Player rolls the 2 dice. If the player rolls a higher value, he wins his bet.
- If player rolls snake eyes (1-1), he receives thrice his bet.
- If both the player and the AI rolls the same value, it�s a draw. This is also applicable with Snake Eyes.
- The game will only end if the player loses all his gold.

Ex 2-1 (Bet)
Create a function that asks the player for his bet and deducts the user�s gold immediately. This function must accept a reference.

Create 2 versions of this function:
A function that returns the bet value and also deducts the user�s gold.
Same as the first function but it must return void

NOTE: Explain the difference of the 3 functions:
int bet(int gold) // You cannot deduct player�s gold. Function is solely used for determining bet
int bet(int& gold) // You can deduct player�s gold and return the bet as normal variable.
void bet(int& gold, int& bet) // You can deduct player�s gold and bet will be set via referencing.

Ex 2-2 (Dice Roll)
Create a function which rolls the 2 dice. This should work for both the player and AI. Since there are 2 dice, they must use reference as parameter.

Ex 2-3 (Payout)
Create a function which evaluates how much the player should win/lose depending on the outcome of the dice roll. Use reference to modify the player�s gold.

Ex 2-4 (Play Round)
Create a playRound function to remove duplicate code. This function will be continuously called in the main function until player loses all his gold. This function must accept the player�s gold as reference.
*/

void betting(int& pBet, int& pGold)
{
	cout << "Place your bet." << endl;
	cin >> pBet;

	//pGold -= pBet;

	cout << "You placed your bet." << endl;
	_getch();
}

void diceRoll(int& playerDice, int& aiDice)
{
	int playerDice1, playerDice2, aiDice1, aiDice2;

	playerDice1 = rand() % 6 + 1;
	playerDice2 = rand() % 6 + 1;
	aiDice1 = rand() % 6 + 1;
	aiDice2 = rand() % 6 + 1;

	playerDice = playerDice1 + playerDice2;
	aiDice = aiDice1 + aiDice2;

	cout << "Player and AI rolling dice..." << endl;
	_getch();
}

void matchResult(int pResult, int aiResult, int pBet, int& pGold)
{
	if (pResult == aiResult)
	{
		cout << "DRAW! Both keep their bet." << endl;
	}
	else if (pResult > aiResult)
	{
		cout << "YOU WIN!" << endl;
		pGold += pBet;
	}
	else if (pResult < aiResult)
	{
		cout << "AI WINS!" << endl;
		pGold -= pBet;
	}
}

int main()
{
	int playerGold = 1000;
	int playerBet = 0;
	int playerResult = 0;
	int aiResult = 0;

	while (playerGold > 0)
	{
		cout << "Player Gold: " << playerGold << endl;

		betting(playerBet, playerGold);

		diceRoll(playerResult, aiResult);

		matchResult(playerResult, aiResult, playerBet, playerGold);

		_getch();

		system("CLS");
	}

	system("PAUSE");

	return 0;

}