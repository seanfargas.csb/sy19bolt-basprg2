#include <iostream>
#include <time.h>
#include <vector>
#include <string>

using namespace std;

//Ex 2 - 1 (Inventory Fill)
//Create a function which fills an inventory of 10 random items.Items in the inventory can be duplicated.
//
//This function must return a vector of string.Each element in the vector represents an item in the inventory.
//
//Here are the items that can be included in the vector.
//RedPotion
//Elixir
//EmptyBottle
//BluePotion
//
//Note that you shouldn�t populate the vector inside the main() function.
//
//Ex 2 - 2 (Display Inventory)
//Create a function which prints the items in the inventory.After creating the vector in Ex 2 - 1, display the items by calling this function.
//
//Pass the inventory to this function.This function does not need write access to the vector.
//Ex 2 - 3 (Count Item)
//Create a function which counts the number of instances an item has appeared in the inventory(vector).
//
//Pass the inventory vector to this function and return an integer which represents the item count.This function does not need write access to the vector.
//
//Ex 2 - 4 (Remove Item)
//Create a function which removes a specific item in the inventory.
//
//Pass the inventory vector to this function.Make sure the reference allows read and write access to the vector

void printVector(vector<string>& inventory)
{
	for (int i = 0; i < inventory.size(); i++)
	{
		cout << inventory[i] << endl;
	}
}

void populateVector(vector<string>& inventory)
{
	// Items in Vector
	inventory.push_back("RedPotion");
	inventory.push_back("Elixir");
	inventory.push_back("EmptyBottle");
	inventory.push_back("BluePotion");

	for (int i = 1; i < 10; i++)
	{
		int inv = rand() % 4 + 1;
		inventory.push_back[inv];
		i++;
	}
}

void removeItem(vector<string>& inventory)
{
	inventory.pop_back();
}

int main()
{
	srand(time(0));

	// Vector
	vector<string> inventory;

	populateVector(inventory);

	printVector(inventory);

	removeItem(inventory);

	system("PAUSE");

	return 0;
}