#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

#ifndef NODE_H
#define NODE_H

#include <string>

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

#endif

void memberNames(Node* head)
{
	Node* current = head;

	for (int i = 1; i <= 5; i++)
	{
		cout << "What's your name soldier?" << endl;
		cin >> current->name;
		current = current->next;
	}
	system("cls");
}

void printLinkedList(Node* head)
{
	Node* current = head;
	while (current != 0)
	{
		cout << current->name << endl;
		current = current->next;
	}
}

void Battle(Node* head)
{
	Node* current = head;
	Node* first = current;
	int i = 0;
	int srand = rand() % 5 + 1;

	cout << current->name << " has drawn " << srand + 1 << "." << endl;

	while (true)
	{
		if (i != srand)
		{
			current = current->next;
			i++;
		}
		else if (i == srand)
		{
			cout << current->name << " was eliminated." << endl;

			Node* temp = current;

			current->next = temp->next;

			delete(temp);

			break;
		}
	}
}

int main()
{
	int rand = (time(0));
	int memberCount;
	string member;

	cout << "How many members do you want to participate in the game?" << endl;
	cin >> memberCount;

	Node* node1 = new Node;
	node1->name = "N1";

	Node* node2 = new Node;
	node2->name = "N2";
	node1->next = node2;

	Node* node3 = new Node;
	node3->name = "N3";
	node2->next = node3;

	Node* node4 = new Node;
	node4->name = "N4";
	node3->next = node4;

	Node* node5 = new Node;
	node5->name = "N5";
	node4->next = node5;
	node5->next = NULL;

	memberNames(node1);

	int membersCount = 5;

	for (int i = 1; membersCount >= 1; i++)
	{
		while (membersCount > 1)
		{
			cout << "==============================" << endl;
			cout << "ROUND " << i << endl;
			cout << "==============================" << endl;

			printLinkedList(node1);

			Battle(node1);

			_getch();
			system("cls");
		}
	}

	system("pause");
	return 0;
}