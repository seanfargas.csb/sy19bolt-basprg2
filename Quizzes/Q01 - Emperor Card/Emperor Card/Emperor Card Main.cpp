#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

void displayCard(vector<string>card)
{
	for (int x = 0; x <= 4; x++)
	{
		cout << card[x] << endl;
	}
}
void cardPick(vector<string>card, int &answer)
{
	cout << "Pick a card to play..." << endl;
	cout << "======================" << endl;
	displayCard(card);
	cin >> answer;
	cout << endl;
	_getch();
	system("CLS");
}
void stats(string &side, int &i, int &cash, int &wager, int &distance)
{
	cout << "Cash: " << cash << endl;
	cout << "Distance left (mm): " << distance << endl;
	cout << "Round: " << i << "/12" << endl;
	cout << "Side: " << side << endl;
	cout << endl;
	cout << "How many mm would you like to wager, Kaiji?" << endl;
	cin >> wager;

	_getch();
	system("CLS");
}
void addCash(int &cash)
{
	cash += 100000;
}
void drillTorture(int &distance, int &wager)
{
	distance = distance - wager;
}
void battle(vector<string> player, vector <string>enemy, int &answer, int &wager, int &cash, int &distance)
{
	int enemyR = rand() + 4; // enemySet
	int pos = answer - 1; // playerSet

	if (pos == 0 && enemyR == 0)
	{
		cout << "Emperor Loses!" << endl;
		cout << endl;
		cout << "You lost! " << endl;
		cout << "DRILLLL DRILLLLL DRILLLLLLLL!  BZZTTT BZZZTTTTTTTT BZZTTTTTTT" << endl;
		drillTorture(distance, wager);

		_getch();
		system("CLS");
	}
	else if (pos >= 1 && enemyR >= 1)
	{
		cout << "[PLAYER] Civilian vs [ENEMY] Civilian" << endl;
		cout << endl;
		cout << "Draw! " << endl;

		_getch();
		system("CLS");

		int enemyR = rand() + 4; // enemySet
		int pos = answer - 1; // playerSet

		if (pos == 0 && enemyR == 0)
		{
			cout << "[PLAYER] Emperor vs [ENEMY] Slave" << endl; // Emperor Loses!
			cout << endl;
			cout << "You lost! " << endl;
			cout << "DRILLLL DRILLLLL DRILLLLLLLL!  BZZTTT BZZZTTTTTTTT BZZTTTTTTT" << endl;
			drillTorture(distance, wager);

			_getch();
			system("CLS");
		}
		else if (pos >= 1 && enemyR >= 1)
		{
			cout << "Pick a card to play..." << endl;
			cout << "======================" << endl;
			cout << "[1] Emperor" << endl;
			cout << "[2] Civilian" << endl;
			cout << "[3] Civilian" << endl;
			cout << "[4] Civilian" << endl;
			cin >> answer;

			_getch();
			system("CLS");

			int enemyR = rand() + 2; // enemySet
			int pos = answer - 1; // playerSet

			if (pos == 0 && enemyR == 0)
			{
				cout << "[PLAYER] Emperor vs [ENEMY] Slave" << endl; // Emperor Loses!
				cout << endl;
				cout << "You lost! " << endl;
				cout << "DRILLLL DRILLLLL DRILLLLLLLL!  BZZTTT BZZZTTTTTTTT BZZTTTTTTT" << endl;
				drillTorture(distance, wager);

				_getch();
				system("CLS");
			}
			else if (pos >= 1 && enemyR >= 1)
			{
				cout << "[PLAYER] Civilian vs [ENEMY] Civilian" << endl;
				cout << endl;
				cout << "Draw! " << endl;

				_getch();
				system("CLS");

				cout << "Pick a card to play..." << endl;
				cout << "======================" << endl;
				cout << "[1] Emperor" << endl;
				cout << "[2] Civilian" << endl;
				cout << "[3] Civilian" << endl;
				cin >> answer;

				cout << "[PLAYER] Civilian vs [ENEMY] Civilian" << endl;
				cout << endl;
				cout << "Draw! " << endl;

				_getch();
				system("CLS");

				int enemyR = rand() + 1; // enemySet
				int pos = answer - 1; // playerSet

				if (pos == 0 && enemyR == 0)
				{
					cout << "[PLAYER] Emperor vs [ENEMY] Slave" << endl; // Emperor Loses!
					cout << endl;
					cout << "You lost! " << endl;
					cout << "DRILLLL DRILLLLL DRILLLLLLLL!  BZZTTT BZZZTTTTTTTT BZZTTTTTTT" << endl;
					drillTorture(distance, wager);

					_getch();
					system("CLS");
				}
				else if (pos >= 1 && enemyR >= 1)
				{
					cout << "[PLAYER] Civilian vs [ENEMY] Civilian" << endl;
					cout << endl;
					cout << "Draw! " << endl;

					_getch();
					system("CLS");

					cout << "Pick a card to play..." << endl;
					cout << "======================" << endl;
					cout << "[1] Emperor" << endl;
					cout << "[2] Civilian" << endl;
					cin >> answer;

					cout << "[PLAYER] Civilian vs [ENEMY] Civilian" << endl;
					cout << endl;
					cout << "Draw! " << endl;

					_getch();
					system("CLS");

					int enemyR = rand() + 0; // enemySet
					int pos = answer - 1; // playerSet

					if (pos == 0 && enemyR == 0)
					{
						cout << "[PLAYER] Emperor vs [ENEMY] Slave" << endl; // Emperor Loses!
						cout << endl;
						cout << "You lost! " << endl;
						cout << "DRILLLL DRILLLLL DRILLLLLLLL!  BZZTTT BZZZTTTTTTTT BZZTTTTTTT" << endl;
						drillTorture(distance, wager);

						_getch();
						system("CLS");
					}
					else if (pos >= 1 && enemyR >= 1)
					{
						cout << "[PLAYER] Civilian vs [ENEMY] Civilian" << endl;
						cout << endl;
						cout << "Draw! " << endl;

						_getch();
						system("CLS");

						cout << "Pick a card to play..." << endl;
						cout << "======================" << endl;
						cout << "[1] Emperor" << endl;
						cin >> answer;

						_getch();
						system("CLS");
					}
					else if (pos == 0 && enemyR > 0)
					{
						cout << "[PLAYER] Emperor vs [ENEMY] Civilian" << endl; //Emperor Wins!
						addCash(cash);

						cout << "You have won " << cash << " !" << endl;

						_getch();
						system("CLS");
					}
					_getch();
					system("CLS");
				}
				else if (pos == 0 && enemyR > 0)
				{
					cout << "[PLAYER] Emperor vs [ENEMY] Civilian" << endl; //Emperor Wins!
					addCash(cash);

					cout << "You have won " << cash << " !" << endl;

					_getch();
					system("CLS");
				}
				_getch();
				system("CLS");
			}
			else if (pos == 0 && enemyR > 0)
			{
				cout << "[PLAYER] Emperor vs [ENEMY] Civilian" << endl; //Emperor Wins!
				addCash(cash);

				cout << "You have won " << cash << " !" << endl;

				_getch();
				system("CLS");
			}
			_getch();
			system("CLS");
		}
		else if (pos == 0 && enemyR > 0)
		{
			cout << "[PLAYER] Emperor vs [ENEMY] Civilian" << endl; //Emperor Wins!
			addCash(cash);

			cout << "You have won " << cash << " !" << endl;

			_getch();
			system("CLS");
		}
	}
	else if (pos == 0 && enemyR > 0)
	{
		cout << "[PLAYER] Emperor vs [ENEMY] Civilian" << endl; //Emperor Wins!
		addCash(cash);

		cout << "You have won " << cash << " !" << endl;

		_getch();
		system("CLS");
	}
}
int main()
{
	vector<string> emperorSet;
	emperorSet.push_back("[1] Emperor");
	emperorSet.push_back("[2] Civilian");
	emperorSet.push_back("[3] Civilian");
	emperorSet.push_back("[4] Civilian");
	emperorSet.push_back("[5] Civilian");

	vector<string> slaveSet;
	slaveSet.push_back("[1] Slave");
	slaveSet.push_back("[2] Civilian");
	slaveSet.push_back("[3] Civilian");
	slaveSet.push_back("[4] Civilian");
	slaveSet.push_back("[5] Civilian");

	int moneyEarned = 0;
	int wager = 0;
	int mmLeft = 30;

	int i = 1;
	for (i = 1; i < 12; i++)
	{
		if (i <= 3)
		{
			string side = "Emperor";
			stats(side, i, moneyEarned, wager, mmLeft);
			cout << "ROUND " << i << endl;
			int answer;
			cardPick(emperorSet, answer);
			battle(emperorSet, slaveSet, answer, moneyEarned, wager, mmLeft);
		}
		else if (i > 3 && i <= 6)
		{
			string side = "Slave";
			stats(side, i, moneyEarned, wager, mmLeft);
			cout << "ROUND " << i << endl;
			int answer;
			cardPick(emperorSet, answer);
			battle(emperorSet, slaveSet, answer, moneyEarned, wager, mmLeft);
		}
		else if (i > 6 && i <= 9)
		{
			string side = "Emperor";
			stats(side, i, moneyEarned, wager, mmLeft);
			cout << "ROUND " << i << endl;
			int answer;
			cardPick(emperorSet, answer);
			battle(emperorSet, slaveSet, answer, moneyEarned, wager, mmLeft);
		}
		else if (i > 9 && i <= 12)
		{
			string side = "Slave";
			stats(side, i, moneyEarned, wager, mmLeft);
			cout << "ROUND " << i << endl;
			int answer;
			cardPick(emperorSet, answer);
			battle(emperorSet, slaveSet, answer, moneyEarned, wager, mmLeft);
		}
	}

	if (moneyEarned >= 20000000)
	{
		cout << "Conratulations! You have reached 20,000,000." << endl;
		cout << "You have won the game." << endl;
	}
	else
	{
		cout << "You did not reach 20,000,000." << endl;
		cout << "You have been defeated." << endl;
	}

	_getch();
	return 0;
}