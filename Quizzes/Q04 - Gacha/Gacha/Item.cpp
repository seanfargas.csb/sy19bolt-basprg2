#include "Item.h"
#include "Unit.h"

Item::Item(string name, int point)
{
	mName = name;
	mPoint = point;
}

string Item::getName()
{
	return mName;
}

int Item::getPoint()
{
	return mPoint;
}

void Item::setName(int name)
{
	mName = name;
}

void Item::setPoint(int point)
{
	mPoint = point;
}
