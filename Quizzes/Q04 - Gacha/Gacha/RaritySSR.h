#pragma once
#include "Item.h"

class RaritySSR :
	public Item
{
public:
	RaritySSR(string name, int point);

	virtual void Effects(Unit* player);
};

