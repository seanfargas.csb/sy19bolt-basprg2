#pragma once
#include <iostream>
#include <string>
#include "Unit.h"

using namespace std;

class Item
{
public:
	Item(string name, int point);

	string getName();
	int getPoint();

	void setName(int name);
	void setPoint(int point);

protected:
	string mName;
	int mPoint;
};

