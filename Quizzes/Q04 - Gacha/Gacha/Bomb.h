#pragma once
#include "Item.h"

class Bomb:
	public Item
{
public:
	Bomb(string name, int point);

	virtual void Effects(Unit* player);
};

