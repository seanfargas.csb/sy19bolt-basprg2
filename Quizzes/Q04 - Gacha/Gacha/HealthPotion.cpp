#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int point) : Item(name, point)
{
}

void HealthPotion::Effects(Unit * player)
{
	cout << "You have pulled a Health Potion!" << endl;
	cout << "You have receieved 30 HP. " << endl;
	player->heal(player);
}
