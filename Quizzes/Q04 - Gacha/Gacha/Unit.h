#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

using namespace std;

class Item;

class Unit
{
public:
	Unit(int hp, int totalCrystal, int totalRarity);

	int hp;
	int totalCrystal;
	int totalRarity;

	int getHp();
	int getTotalCrystal();
	int getTotalRarity();

	vector <Item*> items;

	void R(Unit* player);
	void SR(Unit* player);
	void SSR(Unit* player);
	void heal(Unit* player);
	void bomb(Unit* player);
	void addCrystal(Unit* player);
	void subCrystal(Unit* player);

	int alive();
	void display(Unit* player);
	void game(Unit* player);

private:
	int mHp;
	int mTotalCrystal;
	int mTotalRarity;
};

