#include "Unit.h"
#include "RarityR.h"
#include "RaritySR.h"
#include "RaritySSR.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystal.h"
#include <iostream>

using namespace std;

Unit::Unit(int hp, int totalCrystal, int totalRarity)
{
	mHp = hp;
	mTotalCrystal = totalCrystal;
	mTotalRarity = totalRarity;
	items.push_back(new RarityR("R", 1));
	items.push_back(new RaritySR("SR", 10));
	items.push_back(new RaritySSR("SSR", 50));
	items.push_back(new HealthPotion("HP", 35));
	items.push_back(new Bomb("Bomb", 25));
	items.push_back(new Crystal("Crystal", 15));
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getTotalCrystal()
{
	return mTotalCrystal;
}

int Unit::getTotalRarity()
{
	return mTotalRarity;
}

void Unit::R(Unit * player)
{
	player->mTotalRarity = player->mTotalRarity + 1;
}

void Unit::SR(Unit * player)
{
	player->mTotalRarity = player->mTotalRarity + 10;
}

void Unit::SSR(Unit * player)
{
	player->mTotalRarity = player->mTotalRarity + 50;
}

void Unit::heal(Unit * player)
{
	this->mHp = this->mHp + 30;
}

void Unit::bomb(Unit * player)
{
	this->mHp = this->mHp - 25;
}

void Unit::addCrystal(Unit * player)
{
	player->mTotalCrystal = player->mTotalCrystal + 15;
}

void Unit::subCrystal(Unit * player)
{
	player->mTotalCrystal = player->mTotalCrystal - 5;
}

int Unit::alive()
{
	if (mHp > 0)
	{
		return true;
	}
	else
		return false;
}

void Unit::display(Unit * player)
{
	cout << "HP: " << this->mHp << endl;
	cout << "Crystals: " << this->mTotalCrystal << endl;
	cout << "Rarity Points: " << this->mTotalRarity << endl;
}

void Unit::game(Unit * player)
{
	int itemR = rand() % 100 + 1;
	player->subCrystal(player);

	if (itemR <= 1) // SSR
	{
		player->items[2]; 
	}
	else if ((itemR > 1) && (itemR <= 10)) // SR
	{
		player->items[1]; 
	}
	else if ((itemR > 10) && (itemR <= 50)) // R
	{
		player->items[0]; 
	}
	else if ((itemR > 50) && (itemR <= 55)) // Health Potion
	{
		player->items[3]; 
	}
	else if ((itemR > 55) && (itemR <= 70)) // Bomb
	{
		player->items[4]; 
	}
	else if ((itemR > 70) && (itemR <= 85)) // Crystal
	{
		player->items[5]; 
	}
}
