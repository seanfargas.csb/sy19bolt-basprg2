#pragma once
#include "Item.h"

class HealthPotion:
	public Item
{
public:
	HealthPotion(string name, int point);

	virtual void Effects(Unit* player);
};

