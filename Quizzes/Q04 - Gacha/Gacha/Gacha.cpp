#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "Unit.h"

using namespace std;

int main()
{
	int srand(time(0));
	Unit* player = new Unit(100, 100, 0);

	while (player->alive())
	{
		int i = 0;
		i++;
		player->display(player);
		cout << "Pull: " << i << endl;
		player->game(player);
		system("pause");
		system("cls");
	}
	_getch();
	return 0;
}