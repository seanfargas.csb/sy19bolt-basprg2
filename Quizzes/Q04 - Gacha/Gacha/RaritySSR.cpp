#include "RaritySSR.h"

RaritySSR::RaritySSR(string name, int point): Item(name, point)
{
}

void RaritySSR::Effects(Unit * player)
{
	cout << "You pulled SSR!" << endl;
	cout << "You recieve 50 Rarity Points. " << endl;
	player->SSR(player);
}
