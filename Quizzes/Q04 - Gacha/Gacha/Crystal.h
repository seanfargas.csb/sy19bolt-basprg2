#pragma once
#include "Item.h"

class Crystal:
	public Item
{
public:
	Crystal(string name, int point);

	virtual void Effects(Unit* player);
};

