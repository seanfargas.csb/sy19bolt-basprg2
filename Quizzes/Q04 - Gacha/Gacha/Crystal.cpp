#include "Crystal.h"

Crystal::Crystal(string name, int point) : Item(name, point)
{
}

void Crystal::Effects(Unit * player)
{
	cout << "You pulled Crystal!" << endl;
	cout << "You have recieved 15 Crystals. " << endl;
	player->addCrystal(player);
}
