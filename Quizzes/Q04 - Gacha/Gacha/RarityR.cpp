#include "RarityR.h"

RarityR::RarityR(string name, int point) : Item(name, point)
{
}

void RarityR::Effects(Unit * player)
{
	cout << "You pulled R! " << endl;
	cout << "You recieved 1 Rarity Point. " << endl;
	player->R(player);
}
