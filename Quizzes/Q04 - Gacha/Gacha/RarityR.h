#pragma once
#include "Item.h"

class RarityR:
	public Item
{
public:
	RarityR(string name, int point);

	virtual void Effects(Unit* player);
};

