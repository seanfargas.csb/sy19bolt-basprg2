#include "Bomb.h"

Bomb::Bomb(string name, int point) : Item(name, point)
{
}

void Bomb::Effects(Unit * player)
{
	cout << "Oh no, You have pulled out a Bomb!" << endl;
	cout << "You recieve 25 damage." << endl;
	player->bomb(player);
}
