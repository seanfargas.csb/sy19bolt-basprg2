#include "Mage.h"
#include "Warrior.h"
#include "Assassin.h"

Mage::Mage(string name, string classes, int hp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mClasses = classes;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

Mage::~Mage()
{
}

string Mage::getName()
{
	return this-> mName;
}

string Mage::getClasses()
{
	return this->mClasses;
}

int Mage::getHp()
{
	return this->mHp;
}

int Mage::getPow()
{
	return this->mPow;
}

int Mage::getVit()
{
	return this->mVit;
}

int Mage::getAgi()
{
	return this->mAgi;
}

int Mage::getDex()
{
	return this->mDex;
}

void Mage::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Mage::setName(string name)
{
	mName = name;
}

void Mage::setPow(int value)
{
	/*this->setPow(this->getPow);*/
}

void Mage::attack(Mage * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " hit " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Mage::attack(Warrior * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " hit " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Mage::attack(Assassin * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " hit " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Mage::printStats()
{
	cout << "Name: " << this->getName() << endl; // getname()
	cout << "Class: " << this->getClasses() << endl; //getclass()
	cout << "HP: " << this->getHp() << endl; //gethp()
	cout << "Pow: " << this->getPow() << endl; // getPow()
	cout << "Vit: " << this->getVit() << endl; // getVit()
	cout << "Dex: " << this->getDex() << endl; //getDex()
	cout << "Agi: " << this->getAgi() << endl; //getAgi()
}

void Mage::printVictoryBonus(int addPow)
{
	cout << "Combat ended..." << endl; // getname()
	cout << "Stats increase by..." << endl; //getclass()
	cout << "HP: " << "0" << endl; //gethp()
	cout << "Pow: " << "5" << endl; // getPow()
	cout << "Vit: " << "0" << endl; // getVit()
	cout << "Dex: " "0" << endl; //getDex()
	cout << "Agi: " "0" << endl; //getAgi()

	addPow = 5;
	this->mPow += addPow;
}

void Mage::setDead()
{
	this->mHp = 0;
}

/*void Mage::takeDamage(int damage)
{
	if (damage < 1) return;
	damage = (pow - mvit)*	
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}   */                                                                                                                                                                                                                                  