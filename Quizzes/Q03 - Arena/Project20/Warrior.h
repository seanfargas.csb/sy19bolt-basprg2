#pragma once
#include <string>
#include <iostream>

using namespace std;

class Assassin;
class Mage;

class Warrior
{
public:
	Warrior(string name, string classes, int hp, int pow, int vit, int dex, int agi);
	~Warrior();

	// Getters
	string getName();
	string getClasses();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	// Setters
	void setHp(int value);
	void setName(string name);
	void setPow(int value);

	// Attacks
	void attack(Warrior* target);
	void attack(Assassin* target);
	void attack(Mage* target);

	void takeDamage(int damage);

	// Display
	void printStats();

	// Reward (+3 Power, +3 Vitality)
	void printVictoryBonus(int addPow, int addVit);

	// Dead or Alive
	void setDead();
	inline const bool isDead() const { return this - mHp <= 0; }; // Accessor

private:

	string mClasses;
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

