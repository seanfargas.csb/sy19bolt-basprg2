#include <iostream>
#include <vector>
#include <conio.h>
#include <string>
#include <time.h>
#include "Warrior.h"
#include "Assassin.h"
#include "Mage.h"

using namespace std;

void chooseClass(int x)
{
	// Classes
	// Class = New Class(string name, string class, hp, pow, vit, dex, agi)
	Warrior* warrior = new Warrior("Player", "Warrior", rand()% 20 + 10, rand()% 15 + 1, rand()% 12 + 8, rand()% 10 + 7, rand()% 4 + 3);			// (Name, "Warrior", 30, 15, 20, 17, 7)
	Assassin* assassin = new Assassin("Player", "Assassin", rand()% 8 + 7, rand() % 10 + 1, rand() % 8 + 7, rand() % 7 + 6, rand() % 8 + 7);		// (Name, "Assassin", 15, 10, 15, 13, 15);
	Mage* mage = new Mage("Player", "Mage", rand() % 12 + 10, rand() % 20 + 1, rand() % 6 + 4, rand() % 6 + 3, rand() % 3 + 2);					// (Name, "Mage", 22, 20, 10, 9, 3);

	Warrior* ewarrior = new Warrior("Enemy", "Warrior", rand() % 20 + 10, rand() % 8 + 7, rand() % 12 + 8, rand() % 10 + 7, rand() % 4 + 3);
	Assassin* eassassin = new Assassin("Enemy", "Assassin", rand() % 8 + 7, rand() % 5 + 5, rand() % 8 + 7, rand() % 7 + 6, rand() % 8 + 7);
	Mage* emage = new Mage("Enemy", "Mage", rand() % 12 + 10, rand() % 12 + 8, rand() % 6 + 4, rand() % 6 + 3, rand() % 3 + 2);

	switch (x)
	{
	case 1:
		while (true)
		{
			// Enemy Class Randomizer
			int enemyClass = rand() % 3 + 1;

			bool endCombat = false;

			float hitRating = rand() % 100 + 1;

			if (enemyClass == 1)
			{
				warrior->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				ewarrior->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						warrior->attack(ewarrior);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						ewarrior->attack(warrior);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!warrior->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!ewarrior->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						warrior->printVictoryBonus(3, 3);
					}
					_getch();
				}
			}
			else if (enemyClass == 2)
			{
				warrior->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				eassassin->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						warrior->attack(eassassin);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						eassassin->attack(warrior);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!warrior->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!eassassin->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						warrior->printVictoryBonus(3, 3);
					}
					_getch();
				}
			}
			else
			{
				warrior->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				emage->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						warrior->attack(emage);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						emage->attack(warrior);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!warrior->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!emage->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						warrior->printVictoryBonus(3, 3);
					}
					_getch();
				}
			}
		}
		break;
	case 2:
		while (true)
		{
			// Enemy Class Randomizer
			int enemyClass = rand() % 3 + 1;

			bool endCombat = false;

			float hitRating = rand() % 100 + 1;

			if (enemyClass == 1)
			{
				assassin->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				ewarrior->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						assassin->attack(ewarrior);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						ewarrior->attack(assassin);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!assassin->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!ewarrior->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						assassin->printVictoryBonus(3, 3);
					}
					_getch();
				}
			}
			else if (enemyClass == 2)
			{
				assassin->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				eassassin->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						assassin->attack(eassassin);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						eassassin->attack(assassin);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!assassin->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!eassassin->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						assassin->printVictoryBonus(3, 3);
					}
					_getch();
				}
			}
			else
			{
				assassin->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				emage->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						assassin->attack(emage);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						emage->attack(assassin);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!assassin->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!emage->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						assassin->printVictoryBonus(3, 3);
					}
					_getch();
				}
			}
		}
		break;
	case 3:
		while (true)
		{
			// Enemy Class Randomizer
			int enemyClass = rand() % 3 + 1;

			bool endCombat = false;

			float hitRating = rand() % 100 + 1;

			if (enemyClass == 1)
			{
				mage->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				ewarrior->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						mage->attack(ewarrior);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						ewarrior->attack(mage);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!mage->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!ewarrior->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						mage->printVictoryBonus(5);
					}
					_getch();
				}
			}
			else if (enemyClass == 2)
			{
				mage->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				eassassin->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						mage->attack(eassassin);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						eassassin->attack(mage);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!mage->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!eassassin->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						mage->printVictoryBonus(5);
					}
					_getch();
				}
			}
			else
			{
				mage->printStats();
				cout << endl;
				cout << "VS" << endl;
				cout << endl;
				emage->printStats();
				_getch();
				system("cls");
				while (!endCombat)
				{
					if (hitRating >= 50) // HIT
					{
						mage->attack(emage);
					}
					else // MISS
					{
						cout << "You missed!" << endl;
					}
					_getch();
					if (hitRating >= 50) // HIT
					{
						emage->attack(mage);
					}
					else // MISS
					{
						cout << "Enemy missed!" << endl;
					}
					_getch();
					if (!mage->isDead())
					{
						endCombat = true;
						cout << "You got killed!" << endl;
					}
					else if (!emage->isDead())
					{
						endCombat = true;
						cout << "You have killed the enemy!" << endl;
						_getch();
						system("cls");
						mage->printVictoryBonus(5);
					}
					_getch();
				}
			}
		}
		break;
	default:
		cout << "You did not choose any from the choices. Restart the game and play again." << endl;
	}
}

int main()
{
	srand(time(0));
	string playerName;
	int x;

	cout << "Input the name of your avatar: ";
	cin >> playerName;

	system("CLS");

	cout << "Choose a class: " << endl;
	cout << "               [1] Warrior" << endl;
	cout << "               [2] Assassin" << endl;
	cout << "               [3] Mage" << endl;
	cin >> x;

	system("CLS");

	chooseClass(x);
	
	_getch();
	return 0;
}
