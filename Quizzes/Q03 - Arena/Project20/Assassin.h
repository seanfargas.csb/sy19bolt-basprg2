#pragma once
#include <string>
#include <iostream>

using namespace std;

class Warrior;
class Mage;

class Assassin
{
public:
	Assassin(string name, string classes, int hp, int pow, int vit, int dex, int agi);
	~Assassin();

	// Getters
	string getName();
	string getClasses();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	// Setters
	void setHp(int value);
	void setName(string name);
	void setPow(int value);

	// Attacks
	void attack(Assassin* target);
	void attack(Warrior* target);
	void attack(Mage* target);

	// Display
	void printStats();

	// Reward (+3 Agility, +3 Dexterity)
	void printVictoryBonus(int addAgi, int AddDex);

	// Dead or Alive
	void setDead();
	inline const bool isDead() const { return this - mHp <= 0; }; // Accessor

private:

	string mClasses;
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
};

