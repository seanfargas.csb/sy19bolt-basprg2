#include "Assassin.h"
#include "Warrior.h"
#include "Mage.h"

Assassin::Assassin(string name, string classes, int hp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mClasses = classes;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

Assassin::~Assassin()
{
}

string Assassin::getName()
{
	return this->mName;
}

string Assassin::getClasses()
{
	return this->mClasses;
}

int Assassin::getHp()
{
	return this->mHp;
}

int Assassin::getPow()
{
	return this->mPow;
}

int Assassin::getVit()
{
	return this->mVit;
}

int Assassin::getAgi()
{
	return this->mAgi;
}

int Assassin::getDex()
{
	return this->mDex;
}

void Assassin::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Assassin::setName(string name)
{
	mName = name;
}

void Assassin::setPow(int value)
{
	/*this->setPow(this->getPow);*/
}

void Assassin::attack(Assassin * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " slashed " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Assassin::attack(Warrior * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " slashed " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Assassin::attack(Mage * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " slashed " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Assassin::printStats()
{
	cout << "Name: " << this->getName() << endl; // getname()
	cout << "Class: " << this->getClasses() << endl; //getclass()
	cout << "HP: " << this->getHp() << endl; //gethp()
	cout << "Pow: " << this->getPow() << endl; // getPow()
	cout << "Vit: " << this->getVit() << endl; // getVit()
	cout << "Dex: " << this->getDex() << endl; //getDex()
	cout << "Agi: " << this->getAgi() << endl; //getAgi()
}

void Assassin::printVictoryBonus(int addAgi, int addDex)
{
	cout << "Combat ended..." << endl; // getname()
	cout << "Stats increase by..." << endl; //getclass()
	cout << "HP: " << "0" << endl; //gethp()
	cout << "Pow: " << "0" << endl; // getPow()
	cout << "Vit: " << "0" << endl; // getVit()
	cout << "Dex: " "3" << endl; //getDex()
	cout << "Agi: " "3" << endl; //getAgi()

	addAgi = 3;
	addDex = 3;
	this->mAgi += addAgi;
	this->mDex += addDex;
}

void Assassin::setDead()
{
	this->mHp = 0;
}
