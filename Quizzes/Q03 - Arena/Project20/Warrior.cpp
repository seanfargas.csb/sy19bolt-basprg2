#include "Warrior.h"
#include "Assassin.h"
#include "Mage.h"

Warrior::Warrior(string name, string classes, int hp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mClasses = classes;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

Warrior::~Warrior()
{
}

string Warrior::getName()
{
	return this->mName;
}

string Warrior::getClasses()
{
	return this->mClasses;
}

int Warrior::getHp()
{
	return this->mHp;
}

int Warrior::getPow()
{
	return this->mPow;
}

int Warrior::getVit()
{
	return this->mVit;
}

int Warrior::getAgi()
{
	return this->mAgi;
}

int Warrior::getDex()
{
	return this->mDex;
}

void Warrior::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Warrior::setName(string name)
{
	mName = name;
}

void Warrior::setPow(int value)
{
	/*this->setPow(this->getPow);*/
	mPow = value;
}


void Warrior::attack(Warrior * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " striked " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Warrior::attack(Assassin * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " striked " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Warrior::attack(Mage * target)
{
	target->setHp(target->getHp() - mPow);
	cout << mName << " striked " << target->getName() << ". " << mPow << " damage dealt!" << endl;
}

void Warrior::takeDamage(int damage)
{
	if (damage < 1) return;
	mHp -= damage;
	
	if (mHp <= 0)
	{
		mHp = 0;
		this->setDead();
	}
}

void Warrior::printStats()
{
	cout << "Name: " << this->getName() << endl; // getname()
	cout << "Class: " << this->getClasses() << endl; //getclass()
	cout << "HP: " << this->getHp() << endl; //gethp()
	cout << "Pow: " << this->getPow() << endl; // getPow()
	cout << "Vit: " << this->getVit() << endl; // getVit()
	cout << "Dex: " << this->getDex() << endl; //getDex()
	cout << "Agi: " << this->getAgi() << endl; //getAgi()
}

void Warrior::printVictoryBonus(int addPow, int addVit)
{
	cout << "Combat ended..." << endl; // getname()
	cout << "Stats increase by..." << endl; //getclass()
	cout << "HP: " << "0" << endl; //gethp()
	cout << "Pow: " << "3" << endl; // getPow()
	cout << "Vit: " << "3" << endl; // getVit()
	cout << "Dex: " "0" << endl; //getDex()
	cout << "Agi: " "0" << endl; //getAgi()

	addPow = 3;
	addVit = 3;
	this->mPow += addPow;
	this->mVit += addVit;
}

void Warrior::setDead()
{
	this->mHp = 0;
}

